const express=require("express");
const app=express();
const mongoose=require("mongoose");
const dotenv=require("dotenv");
const bodyParser=require('body-parser');
const path=require('path');
const PUBLIC_KEY_STRIPE=process.env.PUBLIC_KEY_STRIPE;
const SECRET_KEY_STRIPE=process.env.SECRET_KEY_STRIPE;

dotenv.config();

const userRouter=require('./routes/user');
const authRouter=require('./routes/auth');
const productRouter=require("./routes/product");
const cartRouter=require('./routes/cart');
const orderRouter=require('./routes/order');
const stripe=require('stripe')(SECRET_KEY_STRIPE);
const exp = require("constants");
// const port=process.env.PORT || 5000;

mongoose.connect(process.env.MONGO_URL).then(()=>console.log("successfully db connected")).catch((e)=>console.log("error is "+e));
// console.log(port);

app.use(express.json());
// app.use(bodyParser.urlencoded({extended:false}));
// app.use(bodyParser.json());
app.use("/api/auth",authRouter);
app.use("/api/user",userRouter);
app.use("/api/product",productRouter);
app.use("/api/carts",cartRouter);
app.use("/api/orders",orderRouter);
// app.use("/api/user",)

app.set("view engine","hbs");

app.get('/',async(req,res)=>{
    // console.log(key);
    res.render("home",{key:PUBLIC_KEY_STRIPE});
});
app.get("/monish",(req,res)=>{
    res.send("Heloo monish sir");
})
app.post("/payment",async(req,res)=>{
    console.log("coming inside post");
    try{
        stripe.customers.create({
            email:req.body.stripeEmail,
            source:req.body.stripeToken,
            name:'Monish Online Store',
            address:{
                line1:'telephone Exchange Ulhasnagar',
                postal_code:"421001",
                city:'Ulhasnagar',
                country:'India'
            }
        })
        .then((customer)=>{
            return stripe.charges.create({
                amount:2500,
                description:'Buying Some TShirts',
                currency:"USD",
                customer:customer.id
            })
        })
        .then((charge)=>{
            console.log(charge);
            res.send("Success");
        })
        .catch((e)=>{
            res.send(e);
        })
    }catch(e){
        console.log(e);
    }
});
app.listen(process.env.PORT || 5000,()=>{
    console.log("Connected to backend successfully at port 5000")
})