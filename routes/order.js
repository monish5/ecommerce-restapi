const router=require("express").Router();

// const router=require("express").Router();

const {verifyToken,verifyTokenAndAuthorization, verifyTokenAndAdmin}=require('./verifyToken');
const Order=require ('../models/order');
const { route } = require("./user");
const { json } = require("body-parser");
// const products = require("../models/Products");

// Create
router.post("/",verifyToken,async(req,res)=>{
    // console.log(await req.body.json());
    const newOrder=new Order(req.body);

    try{
        const savedOrder=await newOrder.save();
        res.status(200).send(savedOrder);
    }catch(e){
        console.log("e is "+e);
        res.status(500).send(e)
    }
})


// Update
router.put("/:id",verifyTokenAndAdmin,async(req,res)=>{
    // console.log("hiii");
    try{
        const updatedCart=await Cart.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true});
        res.status(200).json(updatedCart);
    }catch(e){res.status(500).json(e)}
})

//delete product
router.delete("/:id",verifyTokenAndAdmin,async(req,res)=>{
    try{
        await Order.findByIdAndDelete(req.params.id);
        res.status(200).send("Order has been Deleted");
    }catch(err){
        res.status(500).send(err);
    }
})
//GET A User Order
router.get("/find/:userId",verifyTokenAndAuthorization,async(req,res)=>{
    try{
        const order=await Order.findOne({userId:req.params.userId});
        // const{password, ...others }=user._doc;
        res.status(200).json(order);
    }catch(err){
        res.status(500).send(err);
    }
})

// GEt All 
router.get("/",verifyTokenAndAdmin,async(req,res)=>{
    try{
        const order=await Order.find();
        res.status(200).json(order);
    }catch(err){
        console.log(err);
        res.status(500).send(err);
    }
})
// Get Monthly Income

router.get("/income",verifyTokenAndAdmin,async(req,res)=>{
    const date=new Date();
    const lastMonth=new Date(date.setMonth(date.getMonth()-1));
    const previousMonth=new Date(date.setMonth(lastMonth.getMonth()-1));
    try{
        const income=await Order.aggregate([
        {
            $match:{createdAt:{$gte:previousMonth}}
        },
        {
            $project:{
                month:{$month:"$createdAt"},
                sales:"$amount"
            }
        },
        {
            $group:{
                _id:"$month",
                total:{$sum:"$sales"},
            }
        }   
        ])
        res.status(200).json(income);
    }catch(e){
        console.log(e);
        res.status(500).send(e);
    }
})


module.exports=router;