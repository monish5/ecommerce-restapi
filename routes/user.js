const router=require("express").Router();
const cryptoJS=require("crypto-js");
const {verifyToken,verifyTokenAndAuthorization, verifyTokenAndAdmin}=require('./verifyToken');
const User=require ('../models/Users');

//Update User
router.put("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    // console.log("hiii");
    if(req.body.password){
        req.body.password=cryptoJS.AES.encrypt(req.body.password,process.env.PASS_SEC).toString()
    }
    try{
        console.log("hiii");
        console.log(req.params.id);
        console.log(req.body);
        console.log(await User.find());
        const updatedUser=await User.findByIdAndUpdate(req.params.id,{$set:{username:req.body.username}},{new:true});
        console.log("Heloo from monish");
        console.log((updatedUser))
        res.status(200).json(updatedUser);
    }catch(e){res.status(500).json(e)}
})


//DELETE
router.delete("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    try{
        await User.findByIdAndDelete(req.params.id);
        res.status(200).send("User has been Deleted");
    }catch(err){
        res.status(500).send(err);
    }
})


//GET
router.get("/find/:id",verifyTokenAndAdmin,async(req,res)=>{
    try{
        const user=await User.findById(req.params.id);
        const{password, ...others }=user._doc;
        res.status(200).json(others);
    }catch(err){
        res.status(500).send(err);
    }
})

// GEt All
router.get("/find",verifyTokenAndAdmin,async(req,res)=>{
    const query=req.query.new;
    try{
        const user=query ? await User.find().sort({_id:1}).limit(2) : await User.find();
        const{password, ...others }=user;
        res.status(200).json(user);
    }catch(err){
        res.status(500).send(err);
    }
})

//  GET STATS
router.get("/stats",verifyTokenAndAdmin,async(req,res)=>{
    const date= new Date();
    const lastyear=date.getFullYear()-1;
    console.log(lastyear+" "+date.getMonth());
    try{
        const data=await User.aggregate([
            {$match:{ createdAt:{$gte:new Date(lastyear,0,1)}}},
            {
                $project:{
                    month:{$month:"$createdAt"}
                }
            },
            {
                $group:{
                    _id:"$month",
                    total:{$sum:1}
                }
            },
            {
                $sort:{total:-1}
            }
        ]);
        console.log("is "+data);
        res.status(200).json(data);
    }catch(err){
        console.log("is "+err);
        res.status(500).send(err);
    }
})


module.exports=router;