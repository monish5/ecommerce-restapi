const router=require("express").Router();
const User=require("../models/Users");
const cryptoJS=require('crypto-js');
const jwt= require("jsonwebtoken");
router.post("/register",async(req,res)=>{
    try{
        const newUser=new User({
        username:req.body.username,
        email:req.body.email,
        password:cryptoJS.AES.encrypt(req.body.password,process.env.PASS_SEC).toString()
    });
    const saveduser=await newUser.save();
    res.status(200).json(saveduser);
}catch(e){
    res.status(400).send("err is "+e)
}
})
router.post("/login",async(req,res)=>{
    try{
        const user=await User.findOne({username:req.body.username});
        // console.log(req.body);
    if(!user){
        res.status(400).send("No user found");
        throw new Error("User Not Found");
    }
    const hashedpw=cryptoJS.AES.decrypt(user.password,process.env.PASS_SEC);
    if(hashedpw.toString(cryptoJS.enc.Utf8)==req.body.password){

        const accessToken=jwt.sign({
            id:user._id,
            isAdmin :user.isAdmin
        },process.env.JWT_SEC,
        {expiresIn:"3d"}
        );
        const {password,...others}=user._doc;
        res.status(200).send({...others,accessToken});
    }
    else{
        res.status(400).send("Password Not Matched");
        throw new Error("Password Not Matched");
    }
    }catch(e){
        res.status(400).send(e);
        console.log("error is "+e);
    }
})

module.exports=router;