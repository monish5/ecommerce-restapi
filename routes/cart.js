const router=require("express").Router();

// const router=require("express").Router();

const {verifyToken,verifyTokenAndAuthorization, verifyTokenAndAdmin}=require('./verifyToken');
const Cart=require ('../models/cart');
const { route } = require("./user");
// const products = require("../models/Products");

// Create
router.post("/",verifyToken,async(req,res)=>{
    const newCart=new Product(req.body);

    try{
        const savedCart=await newCart.save();
        res.status(200).send(savedCart);
    }catch(e){
        res.status(500).send(e)
    }
})


// Update
router.put("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    // console.log("hiii");
    try{
        const updatedCart=await Cart.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true});
        res.status(200).json(updatedCart);
    }catch(e){res.status(500).json(e)}
})

//delete product
router.delete("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    try{
        await Cart.findByIdAndDelete(req.params.id);
        res.status(200).send("Cart has been Deleted");
    }catch(err){
        res.status(500).send(err);
    }
})
//GET A User Cart
router.get("/find/:userId",verifyTokenAndAuthorization,async(req,res)=>{
    try{
        const cart=await Cart.findOne({userId:req.params.userId});
        // const{password, ...others }=user._doc;
        res.status(200).json(cart);
    }catch(err){
        res.status(500).send(err);
    }
})

// GEt All 
router.get("/",verifyTokenAndAdmin,async(req,res)=>{
    try{
        const carts=await Cart.find();
        res.status(200).json(carts);
    }catch(err){
        console.log(err);
        res.status(500).send(err);
    }
})

module.exports=router;