const router=require("express").Router();

const {verifyToken,verifyTokenAndAuthorization, verifyTokenAndAdmin}=require('./verifyToken');
const Product=require ('../models/products');
const { route } = require("./user");
// const products = require("../models/Products");

// Create
router.post("/",verifyTokenAndAdmin,async(req,res)=>{
    const newProduct=new Product(req.body);

    try{
        const savedProduct=await newProduct.save();
        res.status(200).send(savedProduct);
    }catch(e){
        res.status(500).send(e)
    }
})


// Update
router.put("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    // console.log("hiii");
    try{
        // console.log("hiii");
        // console.log(req.params.id);
        // console.log(req.body);
        // console.log(await User.find());
        const updatedProduct=await Product.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true});
        // console.log("Heloo from monish");
        console.log((updatedProduct))
        res.status(200).json(updatedProduct);
    }catch(e){res.status(500).json(e)}
})

//delete product
router.delete("/:id",verifyTokenAndAuthorization,async(req,res)=>{
    try{
        await Product.findByIdAndDelete(req.params.id);
        res.status(200).send("Product has been Deleted");
    }catch(err){
        res.status(500).send(err);
    }
})
//GET A Single Product
router.get("/find/:id",verifyTokenAndAdmin,async(req,res)=>{
    try{
        const product=await Product.findById(req.params.id);
        // const{password, ...others }=user._doc;
        res.status(200).json(product);
    }catch(err){
        res.status(500).send(err);
    }
})

// GEt All Products
router.get("/",verifyTokenAndAdmin,async(req,res)=>{
    const qnew=req.query.new;
    const qcat=req.query.cat;
    var products;
    try{
        if(qnew){
            products=await Product.find().sort({createdAt:1}).limit(5);
        }
        else if(qcat){
            products=await Product.find({categories:{$in:[qcat]}});
        }
        else{
            products=await Product.find();
        }
        res.status(200).json(products);
    }catch(err){
        console.log(err);
        res.status(500).send(err);
    }
})
module.exports=router;